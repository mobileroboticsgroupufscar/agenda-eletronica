** Decisões sobre o que deve ter na agenda **

A agenda será feita no octave e funcionará na IDE através de linhas de comando de texto. (Baseada em linha de texto)


## Contato

O contato deve ter

1. Nome
2. Telefone
3. Endereço
4. e-mail
5. Foto

---

## O que a agenda deve fazer

As funções da agenda são:

1. Criar Contato
2. Editar contato
3. Remover contato
4. Mostrar contato (* agenda toda em ordem alfabética ou contato específico)
5. Salvar a agenda
